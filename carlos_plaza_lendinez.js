const express = require("express"); // es el nostre servidor web
const cors = require('cors'); // ens habilita el cors recordes el bicing???
const bodyParser = require('body-parser'); // per a poder rebre jsons en el body de la resposta
var multer  = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, '../uploads')
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname + '' )
    }
  })
   
  var upload = multer({ storage: storage })

const { request } = require("http");
const { response } = require("express");
const app = express();
const baseUrl = '/miapi';
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false })); //per a poder rebre json en el reuest
app.use(bodyParser.json());
//La configuració de la meva bbdd
ddbbConfig = {
   user: 'carlos-plaza-7e3_rafa',
   host: 'postgresql-carlos-plaza-7e3.alwaysdata.net',
   database: 'carlos-plaza-7e3_carlos',
   password: 'carlos-plaza-7e3-rafa',
   port: 5432
};
//El pool es un congunt de conexions
const Pool = require('pg').Pool
const pool = new Pool(ddbbConfig);

//Exemple endPoint
//Quan accedint a http://localhost:3000/miapi/test   ens saludará
//OBTENER TODAS LAS MOTOS
const getMotos = (request, response) => {
   var consulta = "SELECT * FROM  motos"
   pool.query(consulta, (error, results) => {
       if (error) {
           throw error
       }
       //a qui retornem la el status 200 (OK) i en el cos de la resposta les motos en json
       response.status(200).json(results.rows)
       console.log(results.rows);
   });
}
app.get(baseUrl + '/motos', getMotos);
//FILTRAR MOTOS
const getMotosFiltred = (request, response) => {
    var consulta = "SELECT * FROM  motos where marca='"+request.query.marca+"';";
    pool.query(consulta, (error, results) => {
        if (error) {
            throw error
        }
        //a qui retornem la el status 200 (OK) i en el cos de la resposta les motos en json
        response.status(200).json(results.rows)
    });
 }
 app.get(baseUrl + '/motos/filtro/', getMotosFiltred);

 const deleteMoto=(request,response)=>{
    var borrar="DELETE FROM motos where id="+request.params.eliminar+";";
    pool.query(borrar, (error, results) => {
        if (error) {
            throw error
        } response.status(200).json(results.rows)
    });
}
app.delete(baseUrl+"/motoElimin/:eliminar",deleteMoto);
//AÑADIR MOTO SIN FOTO PROPIA
const addMotoNI=(request,response)=>{
    response.send('Me llego '+JSON.stringify(request.body));
    var insert="INSERT INTO motos VALUES(default,'"+request.body.marca+"','"+request.body.modelo+"','"+request.body.year+"','"+request.body.foto+"','"+request.body.precio+"');";
    pool.query(insert, (error, results) => {
        if (error) {
            throw error
        }else{
        }
        
    });
}
app.post(baseUrl+"/moto",addMotoNI);

//AÑADIR MOTO CON FOTO PROPIA
const addMoto = (request, response) => {
    var insert="INSERT INTO motos VALUES(default,'"+request.body.marca+"','"+request.body.modelo+"','"+request.body.year+"','"+request.body.foto+"','"+request.body.precio+"');";
    pool.query(insert, (error, results) => {
        if (error) {
            throw error
        }else{
        }
        
    });
 }
 app.post(baseUrl + '/moto/foto', addMoto);

 app.post(baseUrl+'/profile', upload.single('foto'), function (req, res, next) {
    //req.file is the `avatar` file
    // req.body will hold the text fields, if there were any
    req.file.path=req.file.originalname;
    var insert="INSERT INTO motos VALUES(default,'"+req.body.marca+"','"+req.body.modelo+"','"+req.body.year+"','http://carlos-plaza-7e3.alwaysdata.net/uploads/"+req.file.originalname+"','"+req.body.precio+"');";
    pool.query(insert, (error, results) => {
        if (error) {
            throw error
        }else{
        }
        
    });
    const file = req.file
  if (!file) {
    const error = new Error('Please upload a file')
    error.httpStatusCode = 400
    return next(error)
  }
    res.send(file)
  });

//Inicialitzem el servei
const PORT = process.env.PORT || 3001; // Port
const IP = process.env.IP || null; // IP

app.listen(PORT, IP, () => {
   console.log("El servidor está inicialitzat en el puerto " + PORT);
});
