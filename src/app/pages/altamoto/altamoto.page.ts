import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-altamoto',
  templateUrl: './altamoto.page.html',
  styleUrls: ['./altamoto.page.scss'],
})
export class AltamotoPage implements OnInit {
marca:string;
modelo:string;
ano:string;
precio:number;
image:any;
  constructor(private router:Router) {
   }

  ngOnInit() {
  }
 async altaMoto() {
   this.image = (<HTMLInputElement>document.getElementsByName("foto")[0]).files[0];
  
    var formData = new FormData();
    formData.append('marca', this.marca);
    formData.append('modelo', this.modelo);
    formData.append('year', this.ano);
    formData.append('foto', this.image);
    formData.append('precio', this.precio.toString());
    console.log(formData);
    fetch("http://carlos-plaza-7e3.alwaysdata.net/miapi/profile", {
      "method": "POST",
      "body": formData
    })
    .then(response => {
        this.router.navigateByUrl('/home');
      });
  }


  volver(){
    this.router.navigate(['home']);
  }

}
