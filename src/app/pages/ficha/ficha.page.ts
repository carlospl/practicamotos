import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-ficha',
  templateUrl: './ficha.page.html',
  styleUrls: ['./ficha.page.scss'],
})
export class FichaPage implements OnInit {
motos:any;
index:number;

  constructor(private route:ActivatedRoute,private router:Router,public alertController: AlertController) {
    this.route.queryParams.subscribe(params=>{
      this.motos=JSON.parse(params.datos);
      this.index=params.index;
    });
   }
  ngOnInit() {
  }
volver(){
  this.router.navigate(['home']);
}

async presentAlert() {
  const alert = await this.alertController.create({
    cssClass: 'my-custom-class',
    header: 'Alert',
    message: 'Estas seguro de querer eliminarlo',
    buttons: [
      {
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
         //accion a hacer
        }
      },
      {
        text: 'Aceptar',
        handler: () => {
          const url = "http://carlos-plaza-7e3.alwaysdata.net/miapi/motoElimin/" + this.motos.id;
          fetch(url, {
            "method": "DELETE"
          })
          .then(response => {
              this.router.navigateByUrl('/home');
              this.conseguido();
            });
        }
      }
    ]
  });

  await alert.present();
}
async conseguido() {
  const alert = await this.alertController.create({
    cssClass: 'my-custom-class',
    header: 'Borrado correctamente',
    buttons: ['OK']
  });

  await alert.present();
}

eliminar(){
  this.presentAlert();
}
}
