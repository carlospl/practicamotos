import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
motos:any[];
filtro:string;
  constructor(private router:Router,private menu: MenuController) {}

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }
  openEnd() {
    this.menu.open('end');
  }
  openCustom() {
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }

  ionViewWillEnter(): void {
    this.getJson("");
  }
  abrir(){
      this.menu.open();
      if(this.menu.isOpen()){
        this.menu.close();
      }
  }
async getJson(marca){
  if(this.menu.isOpen()){
    this.menu.close();
  }
  if(marca==""){
    const respuesta = await fetch(`http://carlos-plaza-7e3.alwaysdata.net/miapi/motos`);
    this.motos= await respuesta.json();
  }else{
    const respuesta = await fetch(`http://carlos-plaza-7e3.alwaysdata.net/miapi/motos/filtro?marca=${marca}`);
    this.motos= await respuesta.json();
  }
}
  
  fichaTecnica(i){
  let navigationExtras: NavigationExtras={
queryParams:{
  datos:JSON.stringify(this.motos[i]),
  index:i
}
  };
  this.router.navigate(['ficha'],navigationExtras);
}
alta(){
  this.router.navigate(['altamoto']);
}


}


//por si no funciona
//https://stackoverflow.com/questions/52885720/ionic-4-getting-error-ng-has-unexpectedly-closed-exit-code-1-when-i-run-i
